use std::{
    net::TcpStream,
    io::{Read, Write},
};
use serde_json::{Value, Result};

const BUFFER_SIZE: usize = 200;

pub struct WrappedSocket {
    stream: TcpStream,
    buffer: [u8; BUFFER_SIZE],
}

impl WrappedSocket {
    pub fn new(s: TcpStream) -> WrappedSocket {
        WrappedSocket { stream: s, buffer: [0; BUFFER_SIZE] }
    }

    pub fn read_message(&mut self) -> std::result::Result<Value, i32> {
        let mut len_bytes: [u8; 8] = [0; 8];

        // Read length from stream:
        // if Ok => do nothing
        // if WouldBlock => Non-terminating error returned (skip task creation)
        // if Error => return terminating error (close connection + remove session)
        match self.stream.read(&mut len_bytes) {
            Ok(_) => (),
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => return Err(0),
            Err(e) => return Err(1),
        };
        

        let mut len: usize = usize::from_be_bytes(len_bytes);
        if len > BUFFER_SIZE {
            len = BUFFER_SIZE;
        }
        match self.stream.read(&mut self.buffer[0..len]) {
            Ok(_) => (),
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => return Err(0),
            Err(e) => return Err(1),
        };

        let res: Result<Value> = serde_json::from_slice(&self.buffer[0..len]);
        match res {
            Ok(val) => return Ok(val),
            Err(_) => return Err(0),
        };
    }
    
    pub fn write_message(&mut self, value: Value) -> std::result::Result<usize, i32> {
        // Convert message to byte array
        let serialized_result: Result<Vec<u8>> = serde_json::to_vec(&value);
        let data: Vec<u8> = match serialized_result {
            Ok(res) => res,
            Err(_) => return Err(0),
        };

        // Send length
        let len: usize = data.len();
        let serialized_len: [u8;8] = len.to_be_bytes();
        match self.stream.write(&serialized_len) {
            Ok(_) => (),
            Err(_) => return Err(1),
        };

        match self.stream.write(&data) {
            Ok(_) => return Ok(len),
            Err(_) => return Err(1),
        };
    }
}
