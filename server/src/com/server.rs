use std::{
    vec::Vec,
    net::{TcpListener, TcpStream},
};
use super::session::Session;

pub struct Server {
    sessions: Vec<Session>,
}

impl Server {
    pub fn new() -> Server {
        let s: Vec<Session> = Vec::<Session>::new();
        Server { sessions: s }
    }

    pub fn start(&mut self) {
        let listener: TcpListener = match TcpListener::bind("127.0.0.1:500") {
            Ok(listener) => listener,
            Err(_) => panic!("Listener bind failed!")
        };

        let mut id: u32 = 0;
        loop {
            let stream: TcpStream = match listener.accept() {
                Ok((_socket, _addr)) => _socket,
                Err(_) => continue,
            };

            let s: Session = Session::new(id, stream);
            self.sessions.push(s);
            id += 1;
        }
    }
}
