use std::{
    net::TcpStream,
    string::String,
};
use super::wsocket::{WrappedSocket};

pub struct Session {
    id: u32,
    socket: WrappedSocket,
    status: String,
}

impl Session {
    pub fn new(id: u32, s: TcpStream) -> Session {
        let socket: WrappedSocket = WrappedSocket::new(s);
        let status: String = "authentication".to_owned();
        Session { id, socket, status }
    }
}
